package dto

import (
	"go-pdf/utils"
	"mime/multipart"
	"strconv"
	"strings"
)

type ApiResponse struct {
	Code    int          `json:"code"`
	Message string       `json:"message"`
	Data    interface{}  `json:"data,omitempty"`
	Errors  []ErrorFiels `json:"errors,omitempty"`
}

type ErrorFiels struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

type GenerateDocumentRequest struct {
	File                           multipart.File
	FileExt                        string
	FileName                       string
	IsProtected                    bool   `json:"is_protected"`
	DocumentPassword               string `json:"document_password"`
	DocumentTitle                  string `json:"document_title"`
	DocumentSubject                string `json:"document_subject"`
	QRcodePosition                 string `json:"qrcode_position"`
	QRcodePositionCustomCordinates bool   `json:"qrcode_position_custom_cordinates"`
	QRcodeCoordinatesX             int    `json:"qrcode_coordinates_x"`
	QRcodeCoordinatesY             int    `json:"qrcode_coordinates_y"`
}

func (req *GenerateDocumentRequest) ReadAndValidateMultipartForm(form *multipart.Form) []ErrorFiels {
	errFields := []ErrorFiels{}

	if len(form.Value["is_protected"]) == 0 {
		return []ErrorFiels{
			{
				Field:   "is_protected",
				Message: "is_protected cannot be empty",
			},
		}
	}

	iProtected, err := strconv.ParseBool(form.Value["is_protected"][0])
	if err != nil {
		return []ErrorFiels{
			{
				Field:   "is_protected",
				Message: "is_protected must be boolean",
			},
		}
	}
	req.IsProtected = iProtected

	if req.IsProtected && len(form.Value["document_password"]) == 0 {
		return []ErrorFiels{
			{
				Field:   "document_password",
				Message: "document_password cannot be empty",
			},
		}
	}
	req.DocumentPassword = form.Value["document_password"][0]

	if len(form.Value["qrcode_position_custom_cordinates"]) == 0 {
		return []ErrorFiels{
			{
				Field:   "qrcode_position_custom_cordinates",
				Message: "qrcode_position_custom_cordinates cannot be empty",
			},
		}
	}

	isQRcodePositionCustomCordinates, err := strconv.ParseBool(form.Value["qrcode_position_custom_cordinates"][0])
	if err != nil {
		return []ErrorFiels{
			{
				Field:   "qrcode_position_custom_cordinates",
				Message: "qrcode_position_custom_cordinates must be boolean",
			},
		}
	}
	req.QRcodePositionCustomCordinates = isQRcodePositionCustomCordinates

	if len(form.Value["qrcode_position"]) == 0 {
		return []ErrorFiels{
			{
				Field:   "qrcode_position",
				Message: "qrcode_position cannot be empty",
			},
		}
	}

	if isQRcodePositionCustomCordinates {
		form.Value["qrcode_position"][0] = strings.ReplaceAll(form.Value["qrcode_position"][0], " ", "")
		coordinates := strings.Split(form.Value["qrcode_position"][0], ",")

		if len(coordinates) < 2 {
			return []ErrorFiels{
				{
					Field:   "qrcode_position",
					Message: "qrcode_position x,y coordinates is not valid",
				},
			}
		}

		x, err := strconv.Atoi(coordinates[0])
		if err != nil {
			return []ErrorFiels{
				{
					Field:   "qrcode_position",
					Message: "qrcode_position x,y coordinates is not valid",
				},
			}
		}
		req.QRcodeCoordinatesX = x

		y, err := strconv.Atoi(coordinates[1])
		if err != nil {
			return []ErrorFiels{
				{
					Field:   "qrcode_position",
					Message: "qrcode_position x,y coordinates is not valid",
				},
			}
		}
		req.QRcodeCoordinatesY = y
	} else {
		if !utils.SliceContains([]string{"BL", "TL", "TR", "BR"}, form.Value["qrcode_position"][0]) {
			return []ErrorFiels{
				{
					Field:   "qrcode_position",
					Message: "qrcode_position must be BL | TL | TR | BR",
				},
			}

		}
		req.QRcodePosition = form.Value["qrcode_position"][0]
	}

	if len(form.Value["document_title"]) != 0 {
		req.DocumentTitle = form.Value["document_title"][0]
	}

	if len(form.Value["document_subject"]) != 0 {
		req.DocumentSubject = form.Value["document_subject"][0]
	}

	return errFields
}
