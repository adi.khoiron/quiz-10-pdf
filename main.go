package main

import (
	"fmt"
	"go-pdf/config"
	"go-pdf/handler"
	"go-pdf/router"
	"go-pdf/server"
	"go-pdf/service"
	"go-pdf/utils"

	"github.com/joho/godotenv"
	"github.com/timemore/foundation/logger"
)

func main() {
	err := utils.ResetStorage()
	if err != nil {
		panic(err)
	}

	log := logger.NewPkgLogger().With().Str("layer", "service").Logger()

	if errEnv := godotenv.Load(); errEnv != nil {
		log.Fatal().Err(errEnv).Msg("Failed to load .env file")
	}

	cfg := config.NewConfig()

	service := service.NewService()
	handler := handler.NewHandler(service)
	router := router.NewRouter(handler)

	server := server.NewServer(cfg, router)

	log.Info().Msg(fmt.Sprintf("Server started at  http://%s:%d", cfg.AppHost, cfg.AppPort))
	log.Fatal().Err(server.ListenAndServe()).Msg("Failed to start the server")
}
