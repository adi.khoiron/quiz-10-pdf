package gopdf

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image"
	"io/ioutil"

	"github.com/yeqown/go-qrcode"
)

func (p *gopdf) GenerateQRcode(base64Image, url, outDir string) (string, error) {
	dec, err := base64.StdEncoding.DecodeString(base64Image)
	if err != nil {
		return "", err
	}

	img, _, err := image.Decode(bytes.NewReader(dec))
	if err != nil {
		return "", err
	}

	qrlogo, err := qrcode.NewWithConfig(
		url,
		qrcode.DefaultConfig(),
		qrcode.WithBuiltinImageEncoder(qrcode.PNG_FORMAT),
		qrcode.WithLogoImage(img),
		qrcode.WithQRWidth(2),
		qrcode.WithCircleShape(),
		qrcode.WithBorderWidth(10),
	)

	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = qrlogo.SaveTo(&buf)
	if err != nil {
		return "", err
	}

	qrCodePath := fmt.Sprintf("%s/qrcode.png", outDir)
	err = ioutil.WriteFile(qrCodePath, buf.Bytes(), 0644)
	if err != nil {
		return "", err
	}

	return qrCodePath, nil
}
