package gopdf

import (
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path/filepath"
)

func (p *gopdf) CreateTempFile(file multipart.File, fileName, outDir string) (string, error) {
	tempFile, err := ioutil.TempFile(outDir, "temp-file-*"+filepath.Ext(fileName))
	if err != nil {
		return "", err
	}
	defer tempFile.Close()

	// Salin isi file multipart ke file temporary
	_, err = io.Copy(tempFile, file)
	if err != nil {
		return "", err
	}

	return tempFile.Name(), nil
}

func (p *gopdf) RemoveFile(filepath string) error {
	return os.Remove(filepath)
}
