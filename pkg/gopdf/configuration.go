package gopdf

func (p *gopdf) ResetCondiguration() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
	}
}

func (p *gopdf) SetDefaultConfiguration() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    "br",
		offset:      []int{-20, 20},
	}
}

// xy = [x, y]
func (p *gopdf) SetOffset(xy []int) {
	p.Configuration.offset = []int{xy[0], xy[1]}
}

func (p *gopdf) SetQrPosition_TL() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    POSITION_TOP_LEFT,
		offset:      []int{20, -20},
	}
}

// set QR POSITION_TOP_RIGHT
func (p *gopdf) SetQrPosition_TR() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    POSITION_TOP_RIGHT,
		offset:      []int{-20, -20},
	}
}

// set QR POSITION_BOTTOM_RIGHT
func (p *gopdf) SetQrPosition_BR() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    POSITION_BOTTOM_RIGHT,
		offset:      []int{-20, 20},
	}
}

// set QR POSITION_BOTTOM_LEFT
func (p *gopdf) SetQrPosition_BL() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    POSITION_BOTTOM_LEFT,
		offset:      []int{20, 20},
	}
}

// set QR POSITION_CENTER
func (p *gopdf) SetQrPosition_C() {
	p.Configuration = configuration{
		scalefactor: 0.11,
		rotate:      0,
		position:    POSITION_CENTER,
		offset:      []int{0, 0},
	}
}
