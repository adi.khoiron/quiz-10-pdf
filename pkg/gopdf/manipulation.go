package gopdf

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/model"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/types"
)

func (p *gopdf) ConvertImageToPDF(inFile, outFile string) error {
	imp, err := api.Import("pos:c, form:A4, scalefactor:0.9", types.POINTS)
	if err != nil {
		return err
	}

	return api.ImportImagesFile([]string{inFile}, outFile, imp, nil)
}

func (p *gopdf) ConvertDocxToPDF(inFile, outFile string) error {
	outFilePath := strings.TrimSuffix(outFile, filepath.Base(outFile))
	err := exec.Command("soffice", "--headless", "--convert-to", "temp.pdf", "--outdir", outFilePath, inFile).Run()
	if err != nil {
		return err
	}

	oldFileName := filepath.Base(inFile)
	outFileName := filepath.Base(outFile)
	oldFileNameWithoutExt := strings.TrimSuffix(oldFileName, filepath.Ext(oldFileName))
	return os.Rename(fmt.Sprintf("%s/%s.temp.pdf", outFilePath, oldFileNameWithoutExt), fmt.Sprintf("%s/%s", outFilePath, outFileName))
}

func (p *gopdf) StampPDF(inFile, outFile string, pages []int, configurations configuration, stampImage string) error {
	var pageStr []string
	for _, page := range pages {
		pageStr = append(pageStr, strconv.Itoa(page))
	}

	config := fmt.Sprintf(
		"rot:%d, pos:%s, scalefactor:%f, off:%d %d",
		configurations.rotate,
		configurations.position,
		configurations.scalefactor,
		configurations.offset[0],
		configurations.offset[1],
	)

	// err := exec.Command("pdfcpu", "stamp", "add", "-pages", "1", "-mode", "image", "--", stampImage, "", p.tempFilePath, outFile).Run().
	// if err != nil {
	// 	return errors.Wrap(err, "exec.Command")
	// }

	m := model.NewDefaultConfiguration()
	if p.password != "" {
		m.UserPW = p.password
	}

	err := api.AddImageWatermarksFile(inFile, outFile, pageStr, false, stampImage, config, m)
	if err != nil {
		return err
	}

	return nil
}

func (p *gopdf) RenameFile(oldpath string, newpath string) error {
	return os.Rename(oldpath, newpath)
}

func (p *gopdf) SetPassword(password string) {
	p.password = password
}

func (p *gopdf) AddPropertiesPDF(inFile, outFile string, prop Properties) error {
	m := model.NewDefaultConfiguration()
	if p.password != "" {
		m.UserPW = p.password
	}

	properties := map[string]string{
		"Title":   prop.Title,
		"Creator": prop.Creator,
		"Author":  prop.Author,
		"Subject": prop.Subject,
	}

	return api.AddPropertiesFile(inFile, outFile, properties, m)
}
