package gopdf

type gopdf struct {
	password      string
	Configuration configuration
	// attributes    Attributes
	// properties    Properties
}

type Properties struct {
	Title   string
	Creator string
	Author  string
	Subject string
}

type configuration struct {
	position    string
	offset      []int
	rotate      int
	scalefactor float64
}

const (
	FILE_JPG  = ".jpg"
	FILE_PNG  = ".png"
	FILE_DOCX = ".docx"
	FILE_PDF  = ".pdf"

	POSITION_TOP_LEFT     = "tl"
	POSITION_TOP_RIGHT    = "tr"
	POSITION_BOTTOM_LEFT  = "bl"
	POSITION_BOTTOM_RIGHT = "br"
	POSITION_CENTER       = "c"
)

func Newgopdf() *gopdf {
	return &gopdf{
		Configuration: configuration{
			scalefactor: 0.11,
			rotate:      0,
			position:    "br",
			offset:      []int{-20, 20},
		},
	}
}
