package service

import (
	"go-pdf/dto"
	"go-pdf/pkg/gopdf"
	"strings"

	"github.com/google/uuid"
	"github.com/pdfcpu/pdfcpu/pkg/api"

	"github.com/pkg/errors"
)

func (s *service) GeneratePDF(data dto.GenerateDocumentRequest) error {
	pdf := gopdf.Newgopdf()
	if data.DocumentPassword != "" {
		pdf.SetPassword(data.DocumentPassword)
	}

	tempFilePath, err := pdf.CreateTempFile(data.File, data.FileName, OutputFileDir+"/temp")
	if err != nil {
		logSvc.Error().Err(err).Msg("can't create temp file")
		return err
	}
	defer pdf.RemoveFile(tempFilePath)

	qrCodePath, err := pdf.GenerateQRcode(PrivyLogo, QrCodeUrl, OutputFileDir+"/qrcode")
	if err != nil {
		logSvc.Error().Err(err).Msg("can't generate QR code")
		return err
	}

	OutputFilePath = OutputFileDir + "/" + uuid.NewString() + gopdf.FILE_PDF

	switch data.FileExt {
	case gopdf.FILE_PDF:
		err := pdf.RenameFile(tempFilePath, OutputFilePath)
		if err != nil {
			logSvc.Error().Err(err).Msg("can't rename file")
			return err
		}

	case gopdf.FILE_JPG, gopdf.FILE_PNG:
		err := pdf.ConvertImageToPDF(tempFilePath, OutputFilePath)
		if err != nil {
			logSvc.Error().Err(err).Msg("can't convert image to PDF")
			return err
		}
	case gopdf.FILE_DOCX:
		err := pdf.ConvertDocxToPDF(tempFilePath, OutputFilePath)
		if err != nil {
			logSvc.Error().Err(err).Msg("can't convert docx to PDF")
			return err
		}
	default:
		return errors.Errorf("file extension %s is not supported", data.FileExt)
	}

	if data.QRcodePositionCustomCordinates {
		pdf.SetQrPosition_C()
		pdf.SetOffset([]int{data.QRcodeCoordinatesX, data.QRcodeCoordinatesY})
	} else {
		switch strings.ToLower(data.QRcodePosition) {
		case gopdf.POSITION_BOTTOM_LEFT:
			pdf.SetQrPosition_BL()
		case gopdf.POSITION_BOTTOM_RIGHT:
			pdf.SetQrPosition_BR()
		case gopdf.POSITION_TOP_LEFT:
			pdf.SetQrPosition_TL()
		case gopdf.POSITION_TOP_RIGHT:
			pdf.SetQrPosition_TR()
		default:
			return errors.Errorf("QR code position %s is not supported", data.QRcodePosition)
		}

	}

	pdfFile, err := api.ReadContextFile(OutputFilePath)
	if err != nil {
		logSvc.Error().Err(err).Msg("can't read file")
		return err
	}

	err = pdf.StampPDF(OutputFilePath, OutputFilePath, []int{pdfFile.PageCount}, pdf.Configuration, qrCodePath)
	if err != nil {
		logSvc.Error().Err(err).Msg("can't stamp PDF")
		return err
	}

	properties := gopdf.Properties{
		Title:   data.DocumentTitle,
		Subject: data.DocumentSubject,
		Author:  "Privy",
		Creator: "PT PRIVY IDENTITAS DIGITAL",
	}
	err = pdf.AddPropertiesPDF(OutputFilePath, OutputFilePath, properties)
	if err != nil {
		logSvc.Error().Err(err).Msg("can't add properties to PDF")
		return err
	}

	return nil
}
