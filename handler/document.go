package handler

import (
	"fmt"
	"go-pdf/dto"
	"go-pdf/pkg/gopdf"
	"go-pdf/utils"
	"net/http"
	"path/filepath"

	"github.com/labstack/echo/v4"
)

func (h *Handler) GeneratePDF(c echo.Context) error {
	reqFile, err := c.FormFile("file")
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "validation error",
			Errors: []dto.ErrorFiels{
				{
					Field:   "file",
					Message: "can't read file",
				},
			},
		})
	}

	reqDTO := dto.GenerateDocumentRequest{}
	if err := reqDTO.ReadAndValidateMultipartForm(c.Request().MultipartForm); len(err) > 0 {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "validation error",
			Errors:  err,
		})
	}

	if reqFile == nil {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "validation error",
			Errors: []dto.ErrorFiels{
				{
					Field:   "file",
					Message: "file is required",
				},
			},
		})
	}

	ext := filepath.Ext(reqFile.Filename)
	if ext == "" {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "validation error",
			Errors: []dto.ErrorFiels{
				{
					Field:   "file",
					Message: "file is not valid",
				},
			},
		})
	}

	if ok := utils.SliceContains([]string{gopdf.FILE_PDF, gopdf.FILE_JPG, gopdf.FILE_PNG, gopdf.FILE_DOCX}, ext); !ok {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "validation error",
			Errors: []dto.ErrorFiels{
				{
					Field:   "file",
					Message: fmt.Sprintf("file extension %s is not allowed", ext),
				},
			},
		})
	}

	// reqDTO.File = reqFile
	// reqDTO.FileExt = ext

	src, err := reqFile.Open()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	defer src.Close()

	reqDTO.File = src
	reqDTO.FileExt = ext
	reqDTO.FileName = reqFile.Filename

	err = h.service.GeneratePDF(reqDTO)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, dto.ApiResponse{
		Code:    http.StatusOK,
		Message: "Success",
	})
}
