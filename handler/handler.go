package handler

import (
	"go-pdf/service"
)

type Handler struct {
	service service.ServiceInterface
}

func NewHandler(service service.ServiceInterface) *Handler {
	return &Handler{
		service: service,
	}
}
