package service_test

import (
	"go-pdf/dto"
	"go-pdf/pkg/gopdf"
	"go-pdf/service"
	"go-pdf/utils"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	utils.SetStoragePath("../storages")
	err := utils.ResetStorage()
	if err != nil {
		panic(err)
	}

	os.Exit(m.Run())
}

func TestImageToPDF(t *testing.T) {
	docSvc := service.NewService()

	t.Run("it should be success generate (convert, stamp, overwrite metadata) from .png to .pdf", func(t *testing.T) {
		ext := ".png"
		file, err := os.Open("example/input" + ext)
		assert.Nil(t, err)

		req := dto.GenerateDocumentRequest{
			File:                           file,
			FileExt:                        ext,
			FileName:                       "input" + ext,
			IsProtected:                    false,
			DocumentPassword:               "",
			DocumentTitle:                  "New Document Title",
			DocumentSubject:                "New Document Subject",
			QRcodePosition:                 gopdf.POSITION_BOTTOM_RIGHT,
			QRcodePositionCustomCordinates: false,
			QRcodeCoordinatesX:             0,
			QRcodeCoordinatesY:             0,
		}

		docSvc.ChangeOutFileDir("../storages/pdf")
		err = docSvc.GeneratePDF(req)
		assert.Nil(t, err)
	})

	t.Run("it should be success generate (convert, stamp, overwrite metadata) from .jpg to .pdf", func(t *testing.T) {
		ext := ".jpg"
		file, err := os.Open("example/input" + ext)
		assert.Nil(t, err)

		req := dto.GenerateDocumentRequest{
			File:                           file,
			FileExt:                        ext,
			FileName:                       "input" + ext,
			IsProtected:                    false,
			DocumentPassword:               "",
			DocumentTitle:                  "New Document Title",
			DocumentSubject:                "New Document Subject",
			QRcodePosition:                 "",
			QRcodePositionCustomCordinates: true,
			QRcodeCoordinatesX:             100,
			QRcodeCoordinatesY:             200,
		}

		docSvc.ChangeOutFileDir("../storages/pdf")
		err = docSvc.GeneratePDF(req)
		assert.Nil(t, err)
	})

}

func TestDocxToPDF(t *testing.T) {
	docSvc := service.NewService()

	t.Run("it should be success generate (convert, stamp, overwrite metadata) from .docx to .pdf", func(t *testing.T) {
		ext := ".docx"
		file, err := os.Open("example/input" + ext)
		assert.Nil(t, err)

		req := dto.GenerateDocumentRequest{
			File:                           file,
			FileExt:                        ext,
			FileName:                       "input" + ext,
			IsProtected:                    false,
			DocumentPassword:               "",
			DocumentTitle:                  "New Document Title",
			DocumentSubject:                "New Document Subject",
			QRcodePosition:                 gopdf.POSITION_TOP_LEFT,
			QRcodePositionCustomCordinates: false,
			QRcodeCoordinatesX:             0,
			QRcodeCoordinatesY:             0,
		}

		docSvc.ChangeOutFileDir("../storages/pdf")
		err = docSvc.GeneratePDF(req)
		assert.Nil(t, err)
	})
}

func TestPDF(t *testing.T) {
	docSvc := service.NewService()

	t.Run("it should be success generate (convert, stamp, overwrite metadata) in .pdf", func(t *testing.T) {
		ext := ".pdf"
		file, err := os.Open("example/input" + ext)
		assert.Nil(t, err)

		req := dto.GenerateDocumentRequest{
			File:                           file,
			FileExt:                        ext,
			FileName:                       "input" + ext,
			IsProtected:                    false,
			DocumentPassword:               "",
			DocumentTitle:                  "New Document Title",
			DocumentSubject:                "New Document Subject",
			QRcodePosition:                 gopdf.POSITION_BOTTOM_LEFT,
			QRcodePositionCustomCordinates: false,
			QRcodeCoordinatesX:             0,
			QRcodeCoordinatesY:             0,
		}

		docSvc.ChangeOutFileDir("../storages/pdf")
		err = docSvc.GeneratePDF(req)
		assert.Nil(t, err)
	})
}

func TestInvalidDocument(t *testing.T) {
	docSvc := service.NewService()

	t.Run("it should be failed generate (convert, stamp, overwrite metadata) .txt document, because document is not supported", func(t *testing.T) {
		ext := ".txt"
		file, err := os.Open("example/input" + ext)
		assert.Nil(t, err)

		req := dto.GenerateDocumentRequest{
			File:                           file,
			FileExt:                        ext,
			FileName:                       "input" + ext,
			IsProtected:                    false,
			DocumentPassword:               "",
			DocumentTitle:                  "New Document Title",
			DocumentSubject:                "New Document Subject",
			QRcodePosition:                 gopdf.POSITION_TOP_RIGHT,
			QRcodePositionCustomCordinates: false,
			QRcodeCoordinatesX:             0,
			QRcodeCoordinatesY:             0,
		}

		docSvc.ChangeOutFileDir("../storages/pdf")
		err = docSvc.GeneratePDF(req)
		assert.Error(t, err)
	})
}
