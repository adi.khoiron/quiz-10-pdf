package utils

import (
	"fmt"
	"os"
	"path/filepath"
)

var (
	// StoragePath is the path to the storage directory
	StoragePath = "storages/pdf"
)

func ResetStorage() error {
	err := filepath.Walk(StoragePath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			err := os.Remove(path)
			if err != nil {
				fmt.Println("Failed to remove file:", path)
				return err
			}
		}

		return nil
	})

	if err != nil {
		fmt.Println("Error:", err)
		return err
	}

	fmt.Println("All files in the directory have been removed.")
	return nil
}

func SetStoragePath(path string) {
	StoragePath = path
}
