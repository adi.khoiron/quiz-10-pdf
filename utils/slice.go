package utils

func SliceContains(array []string, searchValue string) bool {
	for _, value := range array {
		if value == searchValue {
			return true
		}
	}

	return false
}
