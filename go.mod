module go-pdf

go 1.20

require (
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.5.1
	github.com/labstack/echo/v4 v4.10.2
	github.com/pdfcpu/pdfcpu v0.4.1
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.1
	github.com/timemore/foundation v0.0.0-20230324015902-c57664947f7a
	github.com/yeqown/go-qrcode v1.5.10
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/hhrutter/lzw v1.0.0 // indirect
	github.com/hhrutter/tiff v1.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rez-go/stev v0.0.0-20220607035830-a584f4607939 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/rs/zerolog v1.29.1 // indirect
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/yeqown/reedsolomon v1.0.0 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/image v0.5.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
