# document-service-pdf
service to perform pdf manipulation, some features include:
- convert image (.png, .jpg) to .pdf
- convert .docx to .pdf
- generate qr code
- stamp qr code to pdf
- overwrite pdf metadata

### I use existing libs :
- [Godotenv](github.com/joho/godotenv) for read .env config
-	[Echo](github.com/labstack/echo/v4) for routing framework
-	[Testify](github.com/stretchr/testify) for unit testing
- [Google UUID](github.com/google/uuid) for generate uuid
- [Foundation](github.com/timemore/foundation) for logging 
- [Go QR Code](github.com/yeqown/go-qrcode) for generate QR code

### How To Use?
1. Begin by cloning the repository to your local machine.
2. Set up your database.
3. Create an environment file by duplicating the .env.example file and filling in the required values for each service according to your needs.
4. Install the necessary dependencies.
```
go mod tidy
```
5. Run the program
```
go run *.go
```
6. Unit Test
```
go test test/document_test.go -cover  
```
7. Test API using [postman](https://app.getpostman.com/run-collection/18402968-9ccfae5d-ff43-4fbe-9054-398509b4f473?action=collection%2Ffork&source=rip_markdown&collection-url=entityId%3D18402968-9ccfae5d-ff43-4fbe-9054-398509b4f473%26entityType%3Dcollection%26workspaceId%3Da3c53e94-cbc8-4668-9027-b2122261f411) collection